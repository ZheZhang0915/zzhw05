﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _5092_homework5
{
    class EuropeanOption:Option
    {
        public static int cores = Environment.ProcessorCount;
        /*public EurOption(double K,double S,double r,double T, double singma, int trials,int N, bool CV,bool anti,bool muti ):base(K,N,trials,CV,anti,muti)
        {

        }
        */
        public double[] GetPrice(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran, bool anti, bool Deltac,bool muti)
        {
            if (anti && (!Deltac))//USE ANTI TO CALCULATE
            {
                double[] PriceandSE = new double[4];
                double[] path = Simulator.EUSimulateAnti(trials,N, s, k, r, T, sigma, ran,muti);
                double[] path1 = new double[2 * trials ];
                double[] path2 = new double[2 * trials];
                double Callprice = 0;
                double Putprice = 0;
                for (int i = 0; i < 2 * trials; i++)
                {
                    path1[i] = Math.Max(path[i] - k, 0);
                }
                for (int i = 0; i < 2 * trials; i++)
                {
                    path2[i] = Math.Max(k - path[i], 0);
                }
                double total1 = 0;
                double total2 = 0;
                for (int i = 0; i < 2 * trials; i++)
                {
                    total1 = total1 + path1[i];
                    total2 = total2 + path2[i];
                }
                double[] conbine1 = new double[trials];// USE TO CALCULATE ANTI SE FOR CALL
                double[] conbine2 = new double[trials];// USE TO CALCULATE ANTI SE FOR PUT
                for (int i = 0; i < trials; i++)
                {
                    conbine1[i] = (path1[i] + path1[i + trials]) / 2;
                    conbine2[i] = (path2[i] + path2[i + trials]) / 2;
                }
                double mean1 = total1 / 2 / trials;
                double mean2 = total2 / 2 / trials;
                double SEhelper1 = 0;
                double SEhelper2 = 0;
                for (int i = 0; i < trials; i++)
                {
                    SEhelper1 = SEhelper1 + (conbine1[i] - mean1) * (conbine1[i] - mean1);
                    SEhelper2 = SEhelper2 + (conbine2[i] - mean2) * (conbine2[i] - mean2);
                }
                double sdCall = Math.Sqrt(SEhelper1 / (trials - 1) / trials) * Math.Exp(-r * T);
                double sdPut = Math.Sqrt(SEhelper2 / (trials - 1) / trials) * Math.Exp(-r * T);
                Callprice = mean1 * Math.Exp(-r * T);

                Putprice = mean2 * Math.Exp(-r * T);

                PriceandSE[0] = Callprice;
                PriceandSE[1] = sdCall;
                PriceandSE[2] = Putprice;
                PriceandSE[3] = sdPut;
                return PriceandSE;
            }
            else if ((!anti) && (!Deltac))
            {// NOT USE ANTI TO CALCULATE
                double[] PriceandSE = new double[4];
                double[] path = Simulator.EUSimulateNormal(trials, N, s, k, r, T, sigma, ran,muti);
                double[] path1 = new double[trials];
                double[] path2 = new double[trials];
                double Callprice = 0;
                double Putprice = 0;
                for (int i = 0; i < trials; i++)
                {
                    path1[i] = Math.Max(path[i] - k, 0);
                }
                for (int i = 0; i < trials; i++)
                {
                    path2[i] = Math.Max(k - path[i], 0);
                }
                double total1 = 0;
                double total2 = 0;
                for (int i = 0; i < trials; i++)
                {
                    total1 = total1 + path1[i];
                    total2 = total2 + path2[i];
                }
                double mean1 = total1 / trials;
                double mean2 = total2 / trials;
                double SEhelper1 = 0;
                double SEhelper2 = 0;
                for (int i = 0; i < trials; i++)
                {
                    SEhelper1 = SEhelper1 + (path1[i] - mean1) * (path1[i] - mean1);
                    SEhelper2 = SEhelper2 + (path2[i] - mean2) * (path2[i] - mean2);
                }
                double sdCall = Math.Sqrt(SEhelper1 / (trials - 1) / trials) * Math.Exp(-r * T);
                double sdPut = Math.Sqrt(SEhelper2 / (trials - 1) / trials) * Math.Exp(-r * T);
                Callprice = mean1 * Math.Exp(-r * T);
                Putprice = mean2 * Math.Exp(-r * T);

                PriceandSE[0] = Callprice;
                PriceandSE[1] = sdCall;
                PriceandSE[2] = Putprice;
                PriceandSE[3] = sdPut;
                return PriceandSE;
            }
            else if (anti && Deltac)

            {// this is method which use delta hedge and anti 
                    double[] PriceandSE = new double[4];
                    double[,] path = Simulator.SimulateAnti(trials, N, s, k, r, T, sigma, ran, muti);


                    double sum_CTc = 0;
                    double sum_CTc2 = 0;
                    double sum_CTp = 0;
                    double sum_CTp2 = 0;

                    //double[] delta = new double[N];
                    double delta1 = 0;
                    double delta2 = 0;
                    double d1 = 0;
                    double d2 = 0;
                    double CT1 = 0;
                    double CT2 = 0;
                    double Beta1 = -1;
                    for (int i = 0; i < trials; i++)
                    {
                        double cvc1 = 0;
                        double cvc2 = 0;
                        double cvp1 = 0;
                        double cvp2 = 0;

                        for (int j = 0; j < N - 1; j++)
                        {
                            d1 = (Math.Log(path[i, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (N - 1))) / (sigma * Math.Sqrt(T - j * T / (N - 1)));
                            d2 = (Math.Log(path[i + trials, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (N - 1))) / (sigma * Math.Sqrt(T - j * T / (N - 1)));
                            delta1 = CDF.Phi(d1);
                            delta2 = CDF.Phi(d2);
                            cvc1 = delta1 * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (N - 1))) + cvc1;
                            cvc2 = delta2 * (path[i + trials, j + 1] - path[i + trials, j] * Math.Exp(r * T / (N - 1))) + cvc2;
                            cvp1 = (delta1 - 1) * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (N - 1))) + cvp1;
                            cvp2 = (delta2 - 1) * (path[i + trials, j + 1] - path[i + trials, j] * Math.Exp(r * T / (N - 1))) + cvp2;
                        }

                        CT1 = (Math.Max(path[i, N - 1] - k, 0) + Beta1 * cvc1 + Math.Max(path[i + trials, N - 1] - k, 0) + Beta1 * cvc2) * 0.5;
                        CT2 = (Math.Max(k - path[i, N - 1], 0) + Beta1 * cvp1 + Math.Max(k - path[i + trials, N - 1], 0) + Beta1 * cvp2) * 0.5;
                        sum_CTc = sum_CTc + CT1;
                        sum_CTc2 = sum_CTc2 + CT1 * CT1;
                        sum_CTp = sum_CTp + CT2;
                        sum_CTp2 = sum_CTp2 + CT2 * CT2;
                    }
                    double Callprice = sum_CTc / trials * Math.Exp(-r * T);
                    double sdCall = Math.Sqrt((sum_CTc2 - sum_CTc * sum_CTc / trials) * Math.Exp(-2 * r * T) / trials / (trials - 1));
                    double Putprice = sum_CTp / trials * Math.Exp(-r * T);
                    double sdPut = Math.Sqrt((sum_CTp2 - sum_CTp * sum_CTp / trials) * Math.Exp(-2 * r * T) / trials / (trials - 1));


                    PriceandSE[0] = Callprice;
                    PriceandSE[1] = sdCall;
                    PriceandSE[2] = Putprice;
                    PriceandSE[3] = sdPut;
                    return PriceandSE;
                
            }


            else
            {//thisis a method which only use delta hedge
                


                    double[] PriceandSE = new double[4];
                    double[,] path = Simulator.SimulateNormal(trials, N, s, k, r, T, sigma, ran, muti);


                    double sum_CTc = 0;
                    double sum_CTc2 = 0;
                    double sum_CTp = 0;
                    double sum_CTp2 = 0;

                    //double[] delta = new double[N];
                    double delta = 0;
                    double d = 0;
                    double CT1 = 0;
                    double CT2 = 0;
                    double Beta1 = -1;
                    for (int i = 0; i < trials; i++)
                    {
                        double cv1 = 0;
                        double cv2 = 0;

                        for (int j = 0; j < N - 1; j++)
                        {
                            d = (Math.Log(path[i, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (N - 1))) / (sigma * Math.Sqrt(T - j * T / (N - 1)));

                            delta = CDF.Phi(d);
                            cv1 = delta * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (N - 1))) + cv1;
                            cv2 = (delta - 1) * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (N - 1))) + cv2;

                        }
                        CT1 = (Math.Max(path[i, N - 1] - k, 0) + Beta1 * cv1);
                        CT2 = (Math.Max(k - path[i, N - 1], 0) + Beta1 * cv2);
                        sum_CTc = sum_CTc + CT1;
                        sum_CTc2 = sum_CTc2 + CT1 * CT1;
                        sum_CTp = sum_CTp + CT2;
                        sum_CTp2 = sum_CTp2 + CT2 * CT2;
                    }
                    double Callprice = sum_CTc / trials * Math.Exp(-r * T);
                    double sdCall = Math.Sqrt((sum_CTc2 - sum_CTc * sum_CTc / trials) * Math.Exp(-2 * r * T) / trials / (trials - 1));
                    double Putprice = sum_CTp / trials * Math.Exp(-r * T);
                    double sdPut = Math.Sqrt((sum_CTp2 - sum_CTp * sum_CTp / trials) * Math.Exp(-2 * r * T) / trials / (trials - 1));


                    PriceandSE[0] = Callprice;
                    PriceandSE[1] = sdCall;
                    PriceandSE[2] = Putprice;
                    PriceandSE[3] = sdPut;
                    return PriceandSE;
                
               
            }
        }

     /*   public double[] GetGreek(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran, bool Isanti, bool Deltac,bool muti)
        {// USE TO CALCULATE GREEK FOR EUR CALL AND PUT
            Form1 f = new Form1();
            double[] Price1 = GetPrice(trials, N, s, k, r, T, sigma, ran, Isanti, Deltac,muti); 
            double[] Delta1 = GetPrice(trials, N, 1.001 * s, k, r, T, sigma, ran, Isanti, Deltac,muti);
            double[] Gmma1 = GetPrice(trials, N, 0.999 * s, k, r, T, sigma, ran, Isanti, Deltac,muti);
            double[] Theta1 = GetPrice(trials, N, s, k, r, 1.001 * T, sigma, ran, Isanti, Deltac,muti);
            double[] Rho1 = GetPrice(trials, N, s, k, 1.0001 * r, T, sigma, ran, Isanti, Deltac,muti);
            double[] Vega1 = GetPrice(trials, N, s, k, r, T, 1.0001 * sigma, ran, Isanti, Deltac,muti    );
            double callD = (Delta1[0] - Price1[0]) / (0.001 * s);
            double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * s * s);
            double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
            double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
            double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
            double putD = (Delta1[2] - Price1[2]) / (0.001 * s);
            double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * s * s);
            double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
            double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
            double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);

            double[] price = new double[10];

            price[0] = callD;
            price[1] = callG;
            price[2] = callT;
            price[3] = callR;
            price[4] = callV;

            price[5] = putD;
            price[6] = putG;
            price[7] = putT;
            price[8] = putR;
            price[9] = putV;

            return price;//return the result
            f.Update(100);
        }*/
    }
}
