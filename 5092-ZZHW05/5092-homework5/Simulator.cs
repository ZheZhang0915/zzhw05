﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace _5092_homework5
{
    class Simulator
    {
        public static int cores = Environment.ProcessorCount;
        public static double[] EUSimulateNormal(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran,bool muti)
        {//SIMULATE THE STOCK PRICE FOR EUR CALL OR PUT AND NOT USE ANTI 
            double[] EUsimularOP = new double[trials];
            if (!muti)
            {

                for (int i = 0; i < trials; i++)
                {
                    EUsimularOP[i] = s;
                }
                for (int i = 0; i < trials; i++)
                {
                    for (int j = 1; j < N; j++)
                    {
                        EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / ( N - 1)) * ran[i, j]);
                    }

                }
                return EUsimularOP;// RETURN A 1*N MARTIX
            }
            else
            {
                for (int i = 0; i < trials; i++)
                {
                    EUsimularOP[i] = s;
                }
                int trials1;
                if (trials % (cores-1) != 0)
                {
                    trials1 = trials + (cores-1 - trials % (cores-1));
                }
                else
                {
                    trials1 = trials;
                }
                int perc = trials1 / (cores-1);
                Action<object> ranmax = x =>
                {
                    int can = Convert.ToInt32(x);
                    int start = can;
                    int end = Math.Min(start + perc, trials);
                    for (int i = start; i < end; i++)
                    {
                        for (int j = 1; j < N; j++)
                        {
                            EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                        }

                    }
                };
                int number = 0;
                List<Thread> Threadlist = new List<Thread>();
                for (int i = 0; i < cores-1; i++)
                {
                    
                    Thread a = new Thread(new ParameterizedThreadStart(ranmax));
                    Threadlist.Add(a);
                    a.Start(number);
                    number = number + perc;

                }
                foreach (Thread a in Threadlist)
                {
                    a.Join();

                }
                foreach (Thread a  in Threadlist)
                {
                    a.Abort();
                }
                return EUsimularOP;
            }
        }

        public static double[] EUSimulateAnti(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran,bool muti)
        {//SIMULATE THE STOCK PRICE FOR EUR CALLOR PUT AND USE ANTI

            double[] EUsimularOP = new double[2 * trials];
            if (!muti)
            {


                for (int i = 0; i < 2 * trials; i++)
                {
                    EUsimularOP[i] = s;
                }
                for (int i = 0; i < trials; i++)
                {
                    for (int j = 1; j < N; j++)
                    {
                        EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                        EUsimularOP[trials + i] = EUsimularOP[i + trials] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j] * (-1));
                    }

                }
                return EUsimularOP;
            }
            else
            {
                for (int i = 0; i < 2 * trials; i++)
                {
                    EUsimularOP[i] = s;
                }
                int trials1;
                if (trials % (cores-1) != 0)
                {
                    trials1 = trials + (cores-1 - trials % (cores-1));
                }
                else
                {
                    trials1 = trials;
                }
                int perc = trials1 / (cores-1);
                Action<object> ranmax = x =>
                {
                    int can = Convert.ToInt32(x);
                    int start = can;
                    int end = Math.Min(start + perc, trials);
                    for (int i = start; i < end; i++)
                    {
                        for (int j = 1; j < N; j++)
                        {
                            EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                            EUsimularOP[trials + i] = EUsimularOP[i + trials] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j] * (-1));
                        }
                    }

                    
                };
                int number = 0;
                List<Thread> Threadlist = new List<Thread>();
                for (int i = 0; i < cores-1; i++)
                {
                   
                    Thread a = new Thread(new ParameterizedThreadStart(ranmax));
                    Threadlist.Add(a);
                    a.Start(number);
                    number = number + perc;

                }
                foreach (Thread a in Threadlist)
                {
                    a.Join();

                }
                foreach (Thread a in Threadlist)
                {
                    a.Abort();
                }
                return EUsimularOP;
            }

            /* double[] EUsimularOP = new double[n];
            double[] EusimularAnti = new double[2 * n];
            EUsimularOP1 = EUSimulateNormal(t, n, s, k, r, T, sigma, ran);
            for (int i = 0; i < n; i++)
            {
                EusimularAnti[i] = EUsimularOP[i];
                EusimularAnti[i + n] = EUsimularOP[i] * (-1);
            }
            return EusimularAnti;*/
        }

        public static double[,] SimulateNormal(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran,bool muti)
        {
            //SIMULATE THE STOCK PRICE FOR ALL KIND OF CALL OR PUT AND NOT USE ANTI
            double[,] simularOP = new double[trials, N];
            /*double[,] simularT = new double[n, t];
            double[,] simularR = new double[n, t];
            double[,] simularV = new double[n, t];*/

            for (int i = 0; i < trials; i++)
            {
                simularOP[i, 0] = s;
            }
            if (!muti)
            {


                for (int i = 0; i < trials; i++)
                {
                    for (int j = 1; j < N; j++)
                    {
                        simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                        // simulate the path of stock
                        /* simularOP[i+n, j] = simularOP[i+n, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (1.001*T / t) + sigma * Math.Sqrt(1.001*T / t) * ran[i, j]);
                           simularOP[i+2*n, j] = simularOP[i+2*n, j - 1] * Math.Exp((1.0001*r - Math.Pow(sigma, 2) / 2) * (T / t) + sigma * Math.Sqrt(T / t) * ran[i, j]);
                           simularOP[i+3*n, j] = simularOP[i+3*n, j - 1] * Math.Exp((r - Math.Pow(1.0001*sigma, 2) / 2) * (T / t) + 1.0001*sigma * Math.Sqrt(T / t) * ran[i, j]);*/
                    }

                }

                return simularOP;
            }
            else
            {
                int trials1;
                if (trials % (cores-1) != 0)
                {
                    trials1 = trials + (cores-1 - trials % (cores-1));
                }
                else
                {
                    trials1 = trials;
                }
                int perc = trials1 / (cores-1);
                Action<object> ranmax = x =>
                {
                    int can = Convert.ToInt32(x);
                    int start = can;
                    int end = Math.Min(start + perc, trials);
                    for (int i = start; i < end; i++)
                    {
                        for (int j = 1; j < N; j++)
                        {
                            simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                        }


                    }
                };
                int number = 0;
                List<Thread> Threadlist = new List<Thread>();
                    for (int i = 0; i < cores-1; i++)
                    {
                       
                        Thread a = new Thread(new ParameterizedThreadStart(ranmax));
                        Threadlist.Add(a);
                        a.Start(number);
                        number = number + perc;

                    }
                    foreach (Thread a in Threadlist)
                    {
                        a.Join();

                    }
                    foreach (Thread a in Threadlist)
                    {
                        a.Abort();
                    }
                return simularOP;
            }

        }
        public static double[,] SimulateAnti(int trials, int N, double s, double k, double r, double T, double sigma, double[,] ran,bool muti)
        {//SIMULATE THE STOCK PRICE FOR ALL KIND OF CALL OR PUT AND USE ANTI
            double[,] simularOP = new double[2 * trials, N];
            /*double[,] simularT = new double[n, t];
            double[,] simularR = new double[n, t];
            double[,] simularV = new double[n, t];*/

            for (int i = 0; i < 2 * trials; i++)
            {
                simularOP[i, 0] = s;
            }
            if (!muti)
            {


                for (int i = 0; i < trials; i++)
                {
                    for (int j = 1; j < N; j++)
                    {
                        simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                        simularOP[i + trials, j] = simularOP[i + trials, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j] * (-1));
                        // simulate the path of stock
                        /* simularOP[i+n, j] = simularOP[i+n, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (1.001*T / t) + sigma * Math.Sqrt(1.001*T / t) * ran[i, j]);
                           simularOP[i+2*n, j] = simularOP[i+2*n, j - 1] * Math.Exp((1.0001*r - Math.Pow(sigma, 2) / 2) * (T / t) + sigma * Math.Sqrt(T / t) * ran[i, j]);
                           simularOP[i+3*n, j] = simularOP[i+3*n, j - 1] * Math.Exp((r - Math.Pow(1.0001*sigma, 2) / 2) * (T / t) + 1.0001*sigma * Math.Sqrt(T / t) * ran[i, j]);*/
                    }

                }
                return simularOP;
            }
            else
            {
                int trials1;
                if (trials % (cores-1) != 0)
                {
                    trials1 = trials + (cores-1 - trials % (cores-1));
                }
                else
                {
                    trials1 = trials;
                }
                int perc = trials1 / (cores-1);
                Action<object> ranmax = x =>
                {
                    int can = Convert.ToInt32(x);
                    int start = can;
                    int end = Math.Min(start + perc, trials);
                    for (int i = start; i < end; i++)
                    {
                        for (int j = 1; j < N; j++)
                        {
                            simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j]);
                            simularOP[i + trials, j] = simularOP[i + trials, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (N - 1)) + sigma * Math.Sqrt(T / (N - 1)) * ran[i, j] * (-1));
                        }


                    }
                };
                int number = 0;
                List<Thread> Threadlist = new List<Thread>();
                for (int i = 0; i < cores-1; i++)
                {
                 
                    Thread a = new Thread(new ParameterizedThreadStart(ranmax));
                    Threadlist.Add(a);
                    a.Start(number);
                    number = number + perc;

                }
                foreach (Thread a in Threadlist)
                {
                    a.Join();

                }
                foreach (Thread a in Threadlist)
                {
                    a.Abort();
                }
                return simularOP;
            }
        }
    }
}
