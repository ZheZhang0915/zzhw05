﻿namespace _5092_homework5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Stock_price = new System.Windows.Forms.Label();
            this.Strike_price = new System.Windows.Forms.Label();
            this.riskFreerate = new System.Windows.Forms.Label();
            this.vol1 = new System.Windows.Forms.Label();
            this.tenor1 = new System.Windows.Forms.Label();
            this.times = new System.Windows.Forms.Label();
            this.step = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Stockprice = new System.Windows.Forms.TextBox();
            this.vol = new System.Windows.Forms.TextBox();
            this.Kprice = new System.Windows.Forms.TextBox();
            this.rate = new System.Windows.Forms.TextBox();
            this.Tenor = new System.Windows.Forms.TextBox();
            this.Trials = new System.Windows.Forms.TextBox();
            this.steps = new System.Windows.Forms.TextBox();
            this.cores_n = new System.Windows.Forms.TextBox();
            this.antithetic = new System.Windows.Forms.CheckBox();
            this.delta_c = new System.Windows.Forms.CheckBox();
            this.mutithread = new System.Windows.Forms.CheckBox();
            this.theta1 = new System.Windows.Forms.TextBox();
            this.gamma1 = new System.Windows.Forms.TextBox();
            this.delta1 = new System.Windows.Forms.TextBox();
            this.price1 = new System.Windows.Forms.TextBox();
            this.rho1 = new System.Windows.Forms.TextBox();
            this.vega1 = new System.Windows.Forms.TextBox();
            this.se1 = new System.Windows.Forms.TextBox();
            this.call_price = new System.Windows.Forms.Label();
            this.call_delta = new System.Windows.Forms.Label();
            this.call_gamma = new System.Windows.Forms.Label();
            this.call_theta = new System.Windows.Forms.Label();
            this.call_rho = new System.Windows.Forms.Label();
            this.call_vega = new System.Windows.Forms.Label();
            this.call_se = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.runTime = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.textrebate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxlevel = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // Stock_price
            // 
            this.Stock_price.AutoSize = true;
            this.Stock_price.Location = new System.Drawing.Point(35, 122);
            this.Stock_price.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Stock_price.Name = "Stock_price";
            this.Stock_price.Size = new System.Drawing.Size(101, 12);
            this.Stock_price.TabIndex = 0;
            this.Stock_price.Text = "Underlying Price";
            // 
            // Strike_price
            // 
            this.Strike_price.AutoSize = true;
            this.Strike_price.Location = new System.Drawing.Point(35, 155);
            this.Strike_price.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Strike_price.Name = "Strike_price";
            this.Strike_price.Size = new System.Drawing.Size(77, 12);
            this.Strike_price.TabIndex = 1;
            this.Strike_price.Text = "Strike Price";
            // 
            // riskFreerate
            // 
            this.riskFreerate.AutoSize = true;
            this.riskFreerate.Location = new System.Drawing.Point(35, 193);
            this.riskFreerate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.riskFreerate.Name = "riskFreerate";
            this.riskFreerate.Size = new System.Drawing.Size(89, 12);
            this.riskFreerate.TabIndex = 2;
            this.riskFreerate.Text = "Risk-free Rate";
            // 
            // vol1
            // 
            this.vol1.AutoSize = true;
            this.vol1.Location = new System.Drawing.Point(35, 223);
            this.vol1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.vol1.Name = "vol1";
            this.vol1.Size = new System.Drawing.Size(65, 12);
            this.vol1.TabIndex = 3;
            this.vol1.Text = "Volatility";
            // 
            // tenor1
            // 
            this.tenor1.AutoSize = true;
            this.tenor1.Location = new System.Drawing.Point(35, 254);
            this.tenor1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.tenor1.Name = "tenor1";
            this.tenor1.Size = new System.Drawing.Size(35, 12);
            this.tenor1.TabIndex = 4;
            this.tenor1.Text = "Tenor";
            // 
            // times
            // 
            this.times.AutoSize = true;
            this.times.Location = new System.Drawing.Point(35, 288);
            this.times.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.times.Name = "times";
            this.times.Size = new System.Drawing.Size(41, 12);
            this.times.TabIndex = 5;
            this.times.Text = "Trials";
            // 
            // step
            // 
            this.step.AutoSize = true;
            this.step.Location = new System.Drawing.Point(35, 322);
            this.step.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.step.Name = "step";
            this.step.Size = new System.Drawing.Size(35, 12);
            this.step.TabIndex = 6;
            this.step.Text = "Steps";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(451, 247);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(101, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "Number Of Cores:";
            // 
            // Stockprice
            // 
            this.Stockprice.Location = new System.Drawing.Point(140, 119);
            this.Stockprice.Margin = new System.Windows.Forms.Padding(2);
            this.Stockprice.Name = "Stockprice";
            this.Stockprice.Size = new System.Drawing.Size(76, 21);
            this.Stockprice.TabIndex = 8;
            this.Stockprice.Text = "50";
            this.Stockprice.TextChanged += new System.EventHandler(this.Stockprice_TextChanged);
            // 
            // vol
            // 
            this.vol.Location = new System.Drawing.Point(140, 223);
            this.vol.Margin = new System.Windows.Forms.Padding(2);
            this.vol.Name = "vol";
            this.vol.Size = new System.Drawing.Size(76, 21);
            this.vol.TabIndex = 9;
            this.vol.Text = "0.5";
            this.vol.TextChanged += new System.EventHandler(this.vol_TextChanged);
            // 
            // Kprice
            // 
            this.Kprice.Location = new System.Drawing.Point(140, 155);
            this.Kprice.Margin = new System.Windows.Forms.Padding(2);
            this.Kprice.Name = "Kprice";
            this.Kprice.Size = new System.Drawing.Size(76, 21);
            this.Kprice.TabIndex = 10;
            this.Kprice.Text = "60";
            this.Kprice.TextChanged += new System.EventHandler(this.Kprice_TextChanged);
            // 
            // rate
            // 
            this.rate.Location = new System.Drawing.Point(140, 190);
            this.rate.Margin = new System.Windows.Forms.Padding(2);
            this.rate.Name = "rate";
            this.rate.Size = new System.Drawing.Size(76, 21);
            this.rate.TabIndex = 11;
            this.rate.Text = "0.05";
            this.rate.TextChanged += new System.EventHandler(this.rate_TextChanged);
            // 
            // Tenor
            // 
            this.Tenor.Location = new System.Drawing.Point(140, 254);
            this.Tenor.Margin = new System.Windows.Forms.Padding(2);
            this.Tenor.Name = "Tenor";
            this.Tenor.Size = new System.Drawing.Size(76, 21);
            this.Tenor.TabIndex = 12;
            this.Tenor.Text = "1";
            this.Tenor.TextChanged += new System.EventHandler(this.Tenor_TextChanged);
            // 
            // Trials
            // 
            this.Trials.Location = new System.Drawing.Point(140, 285);
            this.Trials.Margin = new System.Windows.Forms.Padding(2);
            this.Trials.Name = "Trials";
            this.Trials.Size = new System.Drawing.Size(76, 21);
            this.Trials.TabIndex = 13;
            this.Trials.Text = "10000";
            this.Trials.TextChanged += new System.EventHandler(this.Trials_TextChanged);
            // 
            // steps
            // 
            this.steps.Location = new System.Drawing.Point(140, 319);
            this.steps.Margin = new System.Windows.Forms.Padding(2);
            this.steps.Name = "steps";
            this.steps.Size = new System.Drawing.Size(76, 21);
            this.steps.TabIndex = 14;
            this.steps.Text = "10";
            this.steps.TextChanged += new System.EventHandler(this.steps_TextChanged);
            // 
            // cores_n
            // 
            this.cores_n.Location = new System.Drawing.Point(561, 244);
            this.cores_n.Margin = new System.Windows.Forms.Padding(2);
            this.cores_n.Name = "cores_n";
            this.cores_n.Size = new System.Drawing.Size(202, 21);
            this.cores_n.TabIndex = 15;
            // 
            // antithetic
            // 
            this.antithetic.AutoSize = true;
            this.antithetic.Location = new System.Drawing.Point(37, 22);
            this.antithetic.Margin = new System.Windows.Forms.Padding(2);
            this.antithetic.Name = "antithetic";
            this.antithetic.Size = new System.Drawing.Size(84, 16);
            this.antithetic.TabIndex = 16;
            this.antithetic.Text = "Antithetic";
            this.antithetic.UseVisualStyleBackColor = true;
            this.antithetic.CheckedChanged += new System.EventHandler(this.antithetic_CheckedChanged);
            // 
            // delta_c
            // 
            this.delta_c.AutoSize = true;
            this.delta_c.Location = new System.Drawing.Point(37, 51);
            this.delta_c.Margin = new System.Windows.Forms.Padding(2);
            this.delta_c.Name = "delta_c";
            this.delta_c.Size = new System.Drawing.Size(114, 16);
            this.delta_c.TabIndex = 17;
            this.delta_c.Text = "Control Variate";
            this.delta_c.UseVisualStyleBackColor = true;
            // 
            // mutithread
            // 
            this.mutithread.AutoSize = true;
            this.mutithread.Location = new System.Drawing.Point(37, 80);
            this.mutithread.Margin = new System.Windows.Forms.Padding(2);
            this.mutithread.Name = "mutithread";
            this.mutithread.Size = new System.Drawing.Size(108, 16);
            this.mutithread.TabIndex = 18;
            this.mutithread.Text = "Multithreading";
            this.mutithread.UseVisualStyleBackColor = true;
            // 
            // theta1
            // 
            this.theta1.Location = new System.Drawing.Point(300, 223);
            this.theta1.Margin = new System.Windows.Forms.Padding(2);
            this.theta1.Name = "theta1";
            this.theta1.Size = new System.Drawing.Size(117, 21);
            this.theta1.TabIndex = 21;
            // 
            // gamma1
            // 
            this.gamma1.Location = new System.Drawing.Point(300, 190);
            this.gamma1.Margin = new System.Windows.Forms.Padding(2);
            this.gamma1.Name = "gamma1";
            this.gamma1.Size = new System.Drawing.Size(117, 21);
            this.gamma1.TabIndex = 22;
            // 
            // delta1
            // 
            this.delta1.Location = new System.Drawing.Point(300, 155);
            this.delta1.Margin = new System.Windows.Forms.Padding(2);
            this.delta1.Name = "delta1";
            this.delta1.Size = new System.Drawing.Size(117, 21);
            this.delta1.TabIndex = 23;
            // 
            // price1
            // 
            this.price1.Location = new System.Drawing.Point(300, 119);
            this.price1.Margin = new System.Windows.Forms.Padding(2);
            this.price1.Name = "price1";
            this.price1.Size = new System.Drawing.Size(117, 21);
            this.price1.TabIndex = 24;
            // 
            // rho1
            // 
            this.rho1.Location = new System.Drawing.Point(300, 254);
            this.rho1.Margin = new System.Windows.Forms.Padding(2);
            this.rho1.Name = "rho1";
            this.rho1.Size = new System.Drawing.Size(117, 21);
            this.rho1.TabIndex = 25;
            // 
            // vega1
            // 
            this.vega1.Location = new System.Drawing.Point(300, 285);
            this.vega1.Margin = new System.Windows.Forms.Padding(2);
            this.vega1.Name = "vega1";
            this.vega1.Size = new System.Drawing.Size(117, 21);
            this.vega1.TabIndex = 26;
            // 
            // se1
            // 
            this.se1.Location = new System.Drawing.Point(300, 319);
            this.se1.Margin = new System.Windows.Forms.Padding(2);
            this.se1.Name = "se1";
            this.se1.Size = new System.Drawing.Size(117, 21);
            this.se1.TabIndex = 27;
            // 
            // call_price
            // 
            this.call_price.AutoSize = true;
            this.call_price.Location = new System.Drawing.Point(238, 122);
            this.call_price.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_price.Name = "call_price";
            this.call_price.Size = new System.Drawing.Size(35, 12);
            this.call_price.TabIndex = 29;
            this.call_price.Text = "Price";
            // 
            // call_delta
            // 
            this.call_delta.AutoSize = true;
            this.call_delta.Location = new System.Drawing.Point(238, 163);
            this.call_delta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_delta.Name = "call_delta";
            this.call_delta.Size = new System.Drawing.Size(35, 12);
            this.call_delta.TabIndex = 30;
            this.call_delta.Text = "Delta";
            // 
            // call_gamma
            // 
            this.call_gamma.AutoSize = true;
            this.call_gamma.Location = new System.Drawing.Point(238, 193);
            this.call_gamma.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_gamma.Name = "call_gamma";
            this.call_gamma.Size = new System.Drawing.Size(35, 12);
            this.call_gamma.TabIndex = 31;
            this.call_gamma.Text = "Gamma";
            // 
            // call_theta
            // 
            this.call_theta.AutoSize = true;
            this.call_theta.Location = new System.Drawing.Point(238, 226);
            this.call_theta.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_theta.Name = "call_theta";
            this.call_theta.Size = new System.Drawing.Size(35, 12);
            this.call_theta.TabIndex = 32;
            this.call_theta.Text = "Theta";
            // 
            // call_rho
            // 
            this.call_rho.AutoSize = true;
            this.call_rho.Location = new System.Drawing.Point(238, 256);
            this.call_rho.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_rho.Name = "call_rho";
            this.call_rho.Size = new System.Drawing.Size(23, 12);
            this.call_rho.TabIndex = 33;
            this.call_rho.Text = "Rho";
            // 
            // call_vega
            // 
            this.call_vega.AutoSize = true;
            this.call_vega.Location = new System.Drawing.Point(238, 288);
            this.call_vega.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_vega.Name = "call_vega";
            this.call_vega.Size = new System.Drawing.Size(29, 12);
            this.call_vega.TabIndex = 34;
            this.call_vega.Text = "Vega";
            // 
            // call_se
            // 
            this.call_se.AutoSize = true;
            this.call_se.Location = new System.Drawing.Point(238, 322);
            this.call_se.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.call_se.Name = "call_se";
            this.call_se.Size = new System.Drawing.Size(17, 12);
            this.call_se.TabIndex = 35;
            this.call_se.Text = "SE";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(451, 285);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(83, 12);
            this.label25.TabIndex = 50;
            this.label25.Text = "Running Time:";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // runTime
            // 
            this.runTime.Location = new System.Drawing.Point(561, 282);
            this.runTime.Margin = new System.Windows.Forms.Padding(2);
            this.runTime.Name = "runTime";
            this.runTime.Size = new System.Drawing.Size(202, 21);
            this.runTime.TabIndex = 51;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(453, 193);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(322, 18);
            this.progressBar1.TabIndex = 52;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(538, 80);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(165, 76);
            this.button1.TabIndex = 53;
            this.button1.Text = "Compute";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(172, 39);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(89, 16);
            this.radioButton1.TabIndex = 54;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Call option";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(172, 70);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(83, 16);
            this.radioButton2.TabIndex = 55;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Put option";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 356);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 56;
            this.label11.Text = "Rebate";
            // 
            // textrebate
            // 
            this.textrebate.Location = new System.Drawing.Point(140, 353);
            this.textrebate.Margin = new System.Windows.Forms.Padding(2);
            this.textrebate.Name = "textrebate";
            this.textrebate.Size = new System.Drawing.Size(76, 21);
            this.textrebate.TabIndex = 57;
            this.textrebate.Text = "5";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(286, 41);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 58;
            this.label9.Text = "Option Type";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "European Option",
            "Asian Option",
            "Digital Option",
            "Barrier Option",
            "Lookback Option",
            "Range Option "});
            this.comboBox1.Location = new System.Drawing.Point(374, 38);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(118, 20);
            this.comboBox1.TabIndex = 59;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(286, 72);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "Barrier Type";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Down and Out",
            "Up and Out",
            "Down and In",
            "Up and In"});
            this.comboBox2.Location = new System.Drawing.Point(374, 69);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(118, 20);
            this.comboBox2.TabIndex = 61;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(35, 389);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 62;
            this.label13.Text = "Barrrier Level";
            // 
            // textBoxlevel
            // 
            this.textBoxlevel.Location = new System.Drawing.Point(140, 386);
            this.textBoxlevel.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxlevel.Name = "textBoxlevel";
            this.textBoxlevel.Size = new System.Drawing.Size(76, 21);
            this.textBoxlevel.TabIndex = 63;
            this.textBoxlevel.Text = "40";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 423);
            this.Controls.Add(this.textBoxlevel);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textrebate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.runTime);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.call_se);
            this.Controls.Add(this.call_vega);
            this.Controls.Add(this.call_rho);
            this.Controls.Add(this.call_theta);
            this.Controls.Add(this.call_gamma);
            this.Controls.Add(this.call_delta);
            this.Controls.Add(this.call_price);
            this.Controls.Add(this.se1);
            this.Controls.Add(this.vega1);
            this.Controls.Add(this.rho1);
            this.Controls.Add(this.price1);
            this.Controls.Add(this.delta1);
            this.Controls.Add(this.gamma1);
            this.Controls.Add(this.theta1);
            this.Controls.Add(this.mutithread);
            this.Controls.Add(this.delta_c);
            this.Controls.Add(this.antithetic);
            this.Controls.Add(this.cores_n);
            this.Controls.Add(this.steps);
            this.Controls.Add(this.Trials);
            this.Controls.Add(this.Tenor);
            this.Controls.Add(this.rate);
            this.Controls.Add(this.Kprice);
            this.Controls.Add(this.vol);
            this.Controls.Add(this.Stockprice);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.step);
            this.Controls.Add(this.times);
            this.Controls.Add(this.tenor1);
            this.Controls.Add(this.vol1);
            this.Controls.Add(this.riskFreerate);
            this.Controls.Add(this.Strike_price);
            this.Controls.Add(this.Stock_price);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Monte Carlo Simulation";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Stock_price;
        private System.Windows.Forms.Label Strike_price;
        private System.Windows.Forms.Label riskFreerate;
        private System.Windows.Forms.Label vol1;
        private System.Windows.Forms.Label tenor1;
        private System.Windows.Forms.Label times;
        private System.Windows.Forms.Label step;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Stockprice;
        private System.Windows.Forms.TextBox vol;
        private System.Windows.Forms.TextBox Kprice;
        private System.Windows.Forms.TextBox rate;
        private System.Windows.Forms.TextBox Tenor;
        private System.Windows.Forms.TextBox Trials;
        private System.Windows.Forms.TextBox steps;
        private System.Windows.Forms.TextBox cores_n;
        private System.Windows.Forms.CheckBox antithetic;
        private System.Windows.Forms.CheckBox delta_c;
        private System.Windows.Forms.CheckBox mutithread;
        private System.Windows.Forms.TextBox theta1;
        private System.Windows.Forms.TextBox gamma1;
        private System.Windows.Forms.TextBox delta1;
        private System.Windows.Forms.TextBox price1;
        private System.Windows.Forms.TextBox rho1;
        private System.Windows.Forms.TextBox vega1;
        private System.Windows.Forms.TextBox se1;
        private System.Windows.Forms.Label call_price;
        private System.Windows.Forms.Label call_delta;
        private System.Windows.Forms.Label call_gamma;
        private System.Windows.Forms.Label call_theta;
        private System.Windows.Forms.Label call_rho;
        private System.Windows.Forms.Label call_vega;
        private System.Windows.Forms.Label call_se;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox runTime;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TextBox textrebate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxlevel;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label9;
    }
}

