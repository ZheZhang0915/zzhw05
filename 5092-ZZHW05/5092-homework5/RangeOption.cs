﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _5092_homework5
{
    class RangeOption:Option
    {
        public static int cores = Environment.ProcessorCount;
        public double[] GetPrice(int trials, int N, double s, double k, double r, double T, double vol, double[,] random, bool antithetic, bool cv, bool multi, bool call)
        {
            double[] optionprice = new double[2];
            double sum = 0, sd = 0, se = 0, sum2 = 0;
            if (trials % cores != 0)
            {
                trials = trials + (cores - trials % cores);
            }

            double[,] CT = new double[trials, 1];
            //generate the simulation matrix including the original one and the antithetical based on geometric brownian motion.         
            int percore = trials / cores;// trials per core needed to be runned.
            // create a function which include the simulation process.
            Action<object> simulation = x =>
            {
                double t = T / (N - 1);
                double cv1 = 0, cv2 = 0, delta1, delta2;
                double erddt = Math.Exp(r * t);
                double nudt = (r - vol * vol / 2) * t;
                double sigsdt = vol * Math.Sqrt(t);
                double[,] sims = new double[trials, N];
                double[,] anti = new double[trials, N];
                int start = Convert.ToInt32(x);
                int end = start + percore;
                cv1 = cv2 = 0;
                for (int i = start; i < end; i++)
                {
                    double max1, max2, min1, min2;
                    max1 = max2 = min1 = min2 = s;
                    cv1 = cv2 = 0;
                    anti[i, 0] = sims[i, 0] = s;//set initial value
                    for (int j = 1; j < N; j++)
                    {
                        sims[i, j] = sims[i, j - 1] * Math.Exp(nudt + sigsdt * random[i, j - 1]);//normal simulate matrix
                        if (sims[i, j] < min1)
                        {
                            min1 = sims[i, j];
                        }
                        if (sims[i, j] > max1)
                        {
                            max1 = sims[i, j];
                        }
                        if (cv)//contral variate 
                        {
                            delta1 = BS_delta(sims[i, j - 1], (N - j) * t, r, vol, k, call);
                            // delta1 = delta(sims[i, j - 1], r, (N - j) * t, vol);
                            cv1 = cv1 + delta1 * (sims[i, j] - sims[i, j - 1] * erddt);
                        }
                        if (antithetic)//antithetic
                        {
                            anti[i, j] = anti[i, j - 1] * Math.Exp(nudt + sigsdt * -1 * random[i, j - 1]);//oposite matrix
                            if (anti[i, j] < min2)
                            {
                                min2 = anti[i, j];
                            }
                            if (anti[i, j] > max2)
                            {
                                max2 = anti[i, j];
                            }
                            if (cv)//control variate of antithetic part
                            {
                                delta2 = BS_delta(anti[i, j - 1], (N - j) * t, r, vol, k, call);
                                cv2 = cv2 + delta2 * (anti[i, j] - anti[i, j - 1] * erddt);
                            }
                        }
                    }
                    // determine the payoff in both method.

                    sims[i, N - 1] = max1 - min1;

                    anti[i, N - 1] = max2 - min2;


                    //calculate the sum of pay-off and the sum of square pay-off in both method.         
                    if (antithetic && !cv)// only antithetic method
                    {
                        CT[i, 0] = (sims[i, N - 1] + anti[i, N - 1]) / 2;
                    }
                    if (cv && !antithetic)//only contral variate
                    {
                        CT[i, 0] = sims[i, N - 1] - cv1;
                    }
                    if (antithetic && cv)//antithetic and contral variate 
                    {
                        CT[i, 0] = (sims[i, N - 1] + anti[i, N - 1] - cv1 - cv2) / 2;
                    }
                    if (!cv && !antithetic)// simple monte carlo simulation
                    {
                        CT[i, 0] = sims[i, N - 1];
                    }
                }
            };
            if (multi)// if we decide to use multithreading 
            {
                int count = 0;
                List<Thread> ThreadList = new List<Thread>();
                //create a bunch of threads according to the cores we get.
                for (int i = 0; i < cores; i++)
                {
                    Thread th = new Thread(new ParameterizedThreadStart(simulation));
                    ThreadList.Add(th);
                    th.Start(count);//pass the start point where each thread start  into the function .
                    count = count + percore;//determine the next start for the next thread.
                }
                foreach (Thread th in ThreadList)
                {
                    th.Join();
                }
                // abort all the thread when we finish.
                foreach (Thread th in ThreadList)
                {
                    th.Abort();
                }
                //use the payoff of every trials we get to calculate the sum and summ of square.
                for (int i = 0; i < trials; i++)
                {
                    sum = sum + CT[i, 0];
                    sum2 = sum2 + CT[i, 0] * CT[i, 0];
                }
                // calculate standard deviation,standard error and option price.
                sd = Math.Sqrt((sum2 - sum * sum / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                optionprice[0] = sum / trials * Math.Exp(-r * T);
                se = sd / Math.Sqrt(trials);
                optionprice[1] = se;
                return optionprice;
            }
            else// if we decide not to use multithreading
            {
                percore = trials;
                //only need to create one thread .
                Thread th = new Thread(new ParameterizedThreadStart(simulation));
                th.Start(0);
                th.Join();
                th.Abort();
                for (int i = 0; i < trials; i++)
                {
                    sum = sum + CT[i, 0];
                    sum2 = sum2 + CT[i, 0] * CT[i, 0];
                }
                // calculate  standard deviation,standard error and option price . 
                sd = Math.Sqrt((sum2 - sum * sum / trials) * Math.Exp(-2 * r * T) / (trials - 1));
                optionprice[0] = sum / trials * Math.Exp(-r * T);
                se = sd / Math.Sqrt(trials);
                optionprice[1] = se;
                return optionprice;
            }
        }
        //delta method generated by black-scholes model
        public double BS_delta(double s, double T, double r, double vol, double k, bool call)
        {
            double d1 = 0, delta = 0;
            d1 = (Math.Log(s / k) + (r + vol * vol / 2.0) * T) / (vol * Math.Sqrt(T));
            if (call)
            {
                delta = CND(d1);
            }
            else if (!call)
            {
                delta = CND(d1) - 1;//a math package which included the cdf function
            }
            return delta;
        }



        static double CND(double d) /// this a method to calculate the cdf
        {
            const double A1 = 0.31938153;
            const double A2 = -0.356563782;
            const double A3 = 1.781477937;
            const double A4 = -1.821255978;
            const double A5 = 1.330274429;
            const double RSQRT2PI = 0.39894228040143267793994605993438;

            double
            K = 1.0 / (1.0 + 0.2316419 * Math.Abs(d));

            double
            cnd = RSQRT2PI * Math.Exp(-0.5 * d * d) *
                  (K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));

            if (d > 0)
                cnd = 1.0 - cnd;

            return cnd;
        }
    }
}
