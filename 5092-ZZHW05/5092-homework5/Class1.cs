﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _5092_homework5
{
    class Class1
    {
        public static int cores = Environment.ProcessorCount;
        private int trials;
        private int N;
        // create a construct which delcare all the variables pass into this class
        public Class1(int trials, int N)
        {
            this.trials = trials;
            this.N = N;
        }
        // use box-muller to generate the random number.
        public static double box_muller(Random rnd)
        {
            double x1, x2, z1, z2;
            x1 = rnd.NextDouble();
            x2 = rnd.NextDouble();
            z1 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Cos(2 * Math.PI * x2);
            z2 = Math.Sqrt(-2 * Math.Log(x1)) * Math.Sin(2 * Math.PI * x2);
            return z1;
        }
        // if we decide not to use the multithreading, use this method.
        public void randomnum()
        {
            Random rnd = new Random();
            for (int i = 0; i <= trials - 1; i++)
            {
                for (int j = 0; j <= N - 1; j++)
                {
                    Form1.ran[i, j] = box_muller(rnd);//pass ramdon value to ramdon matrix.                   
                }
            }
        }
        //multithreading method .
        public void multithread()
        {
            // if the trials devided by the number of threads with remainder, than add a few trials make the trials can be devided without any remainder.
            int trials1;
            if (trials % (cores - 1) != 0)
            {
                trials1 = trials + (cores - 1 - trials % (cores - 1));
            }
            else
            {
                trials1 = trials;
            }
            int perc = trials1 / (cores - 1);
            // create a function which include the process of  generating the random number.
            Action<object> myACt = x => {
                long para = Convert.ToInt32(x);
                Random rnd = new Random((int)DateTime.Now.Ticks);
                long start = para;
                long end =Math.Min ( para + perc,trials);
                for (long i = start; i < end; i++)
                {
                    for (int j = 0; j <= N - 1; j++)
                    {
                        Form1.ran[i, j] = box_muller(rnd);// pass the random number to the random matrix we declared in the form1 class.
                    }
                }
            };
            int count = 0;
            List<Thread> ThreadList = new List<Thread>();
            //create a bunch of threads according to the cores we get.
            for (int i = 0; i < cores-1; i++)
            {
                Thread th = new Thread(new ParameterizedThreadStart(myACt));
                ThreadList.Add(th);
                th.Start(count);//pass the start point where each thread start  into the function .
                count = count + perc;//determine the next start for the next thread.
            }
            
                foreach (Thread th in ThreadList)
                {
                    th.Join();
                }
            // abort all the thread when we finish.
            foreach (Thread th in ThreadList)
            {
                th.Abort();
            }

        }
    }
}
