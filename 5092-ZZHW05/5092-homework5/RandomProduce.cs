﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _5092_homework5
{
    class RandomProduce
    
    {

        public static int cores = Environment.ProcessorCount;
        /*private int trials;
        private int N;
        public RandomProduce(int trials, int N)
        {
            trials = this.trials;
            N = this.N;
        }*/
        public static double ran1 = 0, ran2 = 0;

        public static double[,] GetrandP(int trials, int N,bool muti)
        {
            double[,] rand = new double[trials, N];
            Random rnd = new Random();
            if (!muti)
            {
                double w, c;
                if (trials % 2 == 0)
                {


                    for (int i = 0; i < trials; i = i + 2)
                    {
                        for (int j = 0; j < N; j++)
                        {
                            do
                            {
                                ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                w = ran1 * ran1 + ran2 * ran2;
                            }
                            while (w > 1);
                            c = Math.Sqrt(-2 * Math.Log(w) / w);
                            rand[i, j] = c * ran1;
                            rand[i + 1, j] = c * ran2;
                        }
                    }
                    return rand;
                }
                else
                {

                    for (int i = 0; i < trials - 1; i = i + 2)
                    {
                        for (int j = 0; j < N; j++)
                        {
                            do
                            {
                                ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                w = ran1 * ran1 + ran2 * ran2;
                            }
                            while (w > 1);
                            c = Math.Sqrt(-2 * Math.Log(w) / w);
                            rand[i, j] = c * ran1;
                            rand[i + 1, j] = c * ran2;
                        }
                    }
                    for (int j = 0; j < N; j++)
                    {
                        do
                        {
                            ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                            ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                            w = ran1 * ran1 + ran2 * ran2;
                        }
                        while (w > 1);
                        c = Math.Sqrt(-2 * Math.Log(w) / w);
                        rand[trials - 1, j] = c * ran1;

                    }
                    return rand;
                }
            }
            else
            {
                double w, c;
                int trials1;
                if (trials%cores!=0)
                {
                    trials1 = trials + (cores - trials % cores);
                }
                else
                {
                    trials1 = trials;
                }
                int perc = trials1 / cores;
                Action<object> ran = x =>
                {

                    int z = Convert.ToInt32(x);
                    int start = z;
                    int end = Math.Min(start + perc, trials);
                    if (perc % 2 == 0)
                    {


                        for (int i = start; i < end; i = i + 2)
                        {
                            for (int j = 0; j < N; j++)
                            {
                                do
                                {
                                    ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                    ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                    w = ran1 * ran1 + ran2 * ran2;
                                }
                                while (w > 1);
                                c = Math.Sqrt(-2 * Math.Log(w) / w);
                                rand[i, j] = c * ran1;
                                rand[i + 1, j] = c * ran2;
                            }
                        }

                    }
                    else
                    {
                        for (int i = start; i < end - 1; i = i + 2)
                        {
                            for (int j = 0; j < N; j++)
                            {
                                do
                                {
                                    ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                    ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                    w = ran1 * ran1 + ran2 * ran2;
                                }
                                while (w > 1);
                                c = Math.Sqrt(-2 * Math.Log(w) / w);
                                rand[i, j] = c * ran1;
                                rand[i + 1, j] = c * ran2;
                            }
                        }
                        for (int j = 0; j < N; j++)
                        {
                            do
                            {
                                ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                                w = ran1 * ran1 + ran2 * ran2;
                            }
                            while (w > 1);
                            c = Math.Sqrt(-2 * Math.Log(w) / w);
                            rand[end - 1, j] = c * ran1;

                        }
                    }
                };
                int number = 0;
                List<Thread> Threadlist = new List<Thread>();
                for (int i = 0; i < cores; i++)
                {
                    Thread a = new Thread(new ParameterizedThreadStart(ran));
                    Threadlist.Add(a);
                    a.Start(number);
                    number = number + perc;
                }
                
                    foreach (Thread a in Threadlist)
                    {
                        a.Join();
                    }
                foreach (Thread a in Threadlist)
                {
                    a.Abort();
                }
                return rand;
            }
        }
        public static double[,] GeteandB(int trials, int N)
        {
            double[,] rand = new double[trials, N];
            double z1, z2;
            Random rnd = new Random();
            if (trials % 2 == 0)
            {
                for (int i = 0; i < trials; i = i + 2)
                {
                    for (int j = 0; j < N; j++)
                    {
                       
                            ran1 = rnd.NextDouble() ;
                            ran2 = rnd.NextDouble();                        
                        z1 = Math.Sqrt(-2 * Math.Log(ran1)) * Math.Cos(2 * Math.PI * ran2);
                        z2 = Math.Sqrt(-2 * Math.Log(ran1)) * Math.Sin(2 * Math.PI * ran2);                        
                        rand[i, j] = z1;
                        rand[i + 1, j] = z2;
                    }
                }
                return rand;
            }
            else
            {

                for (int i = 0; i < trials - 1; i = i + 2)
                {
                    for (int j = 0; j < N; j++)
                    {

                        ran1 = rnd.NextDouble();
                        ran2 = rnd.NextDouble();
                        z1 = Math.Sqrt(-2 * Math.Log(ran1)) * Math.Cos(2 * Math.PI * ran2);
                        z2 = Math.Sqrt(-2 * Math.Log(ran1)) * Math.Sin(2 * Math.PI * ran2);
                        rand[i, j] = z1;
                        rand[i + 1, j] = z2;
                    }
                }
                for (int j = 0; j < N; j++)
                {

                    ran1 = rnd.NextDouble();
                    ran2 = rnd.NextDouble();
                    z1 = Math.Sqrt(-2 * Math.Log(ran1)) * Math.Cos(2 * Math.PI * ran2);
                    rand[trials - 1, j] =z1;

                }
                return rand;
            }
}

            }


    }

